﻿#include "ItemAsset.h"

UItemAsset::UItemAsset() {}

EDataValidationResult UItemAsset::IsDataValid(TArray<FText>& ValidationErrors)
{
	if (Mesh == nullptr)
	{
		ValidationErrors.Add(FText::FromString("Mesh is null"));
		return EDataValidationResult::Invalid;
	}
	return EDataValidationResult::Valid;
}
