﻿#pragma once

#include "CoreMinimal.h"

#include "ItemAsset.generated.h"

UCLASS(Blueprintable, BlueprintType)
class QTAQ_API UItemAsset : public UDataAsset
{
	GENERATED_BODY()

public:
	UItemAsset();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Description;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TObjectPtr<class UStaticMesh> Mesh;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<TObjectPtr<class UMaterialInterface>> Materials;

protected:
#if WITH_EDITOR
	virtual EDataValidationResult IsDataValid(TArray<FText>& ValidationErrors) override;
#endif // WITH_EDITOR

};
