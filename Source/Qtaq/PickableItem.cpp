﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "PickableItem.h"

#include "ItemAsset.h"


APickableItem::APickableItem()
{
	PrimaryActorTick.bCanEverTick = false;
	Root = RootComponent = CreateDefaultSubobject<USceneComponent>("Root");
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	Mesh->SetupAttachment(RootComponent);
}

EDataValidationResult APickableItem::IsDataValid(TArray<FText>& ValidationErrors)
{
	if (ItemType == nullptr)
	{
		ValidationErrors.Add(FText::FromString("Item type must be set"));
		return EDataValidationResult::Invalid;
	}	
	return EDataValidationResult::Valid;
}

void APickableItem::SetItemType(UItemAsset* NewItemType)
{
	check(NewItemType != nullptr);
	ItemType = NewItemType;
	Mesh->SetStaticMesh(ItemType->Mesh);
	size_t Idx = 0;
	for (auto& Material: NewItemType->Materials)
	{
		Mesh->SetMaterial(Idx, Material);
		Idx++;
	}
}

void APickableItem::BeginPlay()
{
	Super::BeginPlay();
	
}

void APickableItem::OnConstruction(const FTransform& Transform)
{
	if(ItemType == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("ItemType is not set"));
		return;
	}
	Mesh->SetStaticMesh(ItemType->Mesh);
	size_t Idx = 0;
	for (auto& Material: ItemType->Materials)
	{
		Mesh->SetMaterial(Idx, Material);
		Idx++;
	}
}
