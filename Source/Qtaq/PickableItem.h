﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PickableItem.generated.h"

UCLASS()
class QTAQ_API APickableItem : public AActor
{
	GENERATED_BODY()

public:
	APickableItem();

protected:
#if WITH_EDITOR
	virtual EDataValidationResult IsDataValid(TArray<FText>& ValidationErrors) override;
#endif // WITH_EDITOR

	virtual void OnConstruction(const FTransform& Transform) override;
	
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintSetter)
	void SetItemType(class UItemAsset* ItemType);

private:
	UPROPERTY(EditAnywhere, BlueprintSetter=SetItemType)
	TObjectPtr<class UItemAsset> ItemType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess))
	TObjectPtr<class USceneComponent> Root;

	UPROPERTY()
	TObjectPtr<class UStaticMeshComponent> Mesh;
};
