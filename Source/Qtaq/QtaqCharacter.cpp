// Fill out your copyright notice in the Description page of Project Settings.


#include "QtaqCharacter.h"

#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "DrawDebugHelpers.h"

AQtaqCharacter::AQtaqCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	Collision = CreateDefaultSubobject<UCapsuleComponent>("Collision");
	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>("SkeletalMesh");
	CameraArm = CreateDefaultSubobject<USpringArmComponent>("CameraArm");
	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");

	RootComponent = Collision;

	SkeletalMesh->SetupAttachment(Collision);
	CameraArm->SetupAttachment(Collision);
	Camera->SetupAttachment(CameraArm);
}

void AQtaqCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

void AQtaqCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	// gravity
	AddActorLocalOffset(DeltaTime * FVector{0.0, 0.0, -980.0}, true);

}

void AQtaqCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	auto* Input = Cast<UEnhancedInputComponent>(PlayerInputComponent);
	check(Input);

	if (auto* PlayerController = Cast<APlayerController>(GetController()))
	{
		UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer());
		Subsystem->AddMappingContext(DefaultInputContext, INT32_MAX);
	}

	Input->BindAction(MoveAction, ETriggerEvent::Triggered, this, &AQtaqCharacter::Move);
	Input->BindAction(LookRotateAction, ETriggerEvent::Triggered, this, &AQtaqCharacter::LookRotate);
}

void AQtaqCharacter::Move(const FInputActionValue& Value)
{
	auto V = Value.Get<FInputActionValue::Axis2D>();
	V *= Speed;

	FVector Offset{V.X, V.Y, 0.0};
	
	FHitResult Hit;
	AddActorLocalOffset(GetWorld()->GetDeltaSeconds() * Offset, true, &Hit);
	if(Hit.IsValidBlockingHit())
	{
		auto WorldOffset = GetTransform().TransformVector(Offset);
		auto Up = Hit.Normal.Cross(WorldOffset);
		auto SlideDir = -Hit.Normal.Cross(Up).GetSafeNormal();
		auto ProjOffset =  WorldOffset.ProjectOnToNormal(SlideDir);

#if !UE_BUILD_SHIPPING
		if (ShouldDrawDebugVectorsOnCollision)
		{
			DrawDebugLine(GetWorld(), GetActorLocation(), GetActorLocation() + WorldOffset, FColor::White, false, -1, 255);
			DrawDebugLine(GetWorld(), Hit.ImpactPoint, Hit.ImpactPoint + Hit.Normal * 50, FColor::Yellow, false, -1, 255);
			DrawDebugLine(GetWorld(), Hit.ImpactPoint, Hit.ImpactPoint + Up, FColor::Cyan, false, -1, 255);
			DrawDebugLine(GetWorld(), Hit.ImpactPoint, Hit.ImpactPoint + SlideDir * 50, FColor::Orange, false, -1, 255);
			DrawDebugLine(GetWorld(), GetActorLocation(), GetActorLocation() + ProjOffset, FColor::Red, false, -1, 255);
		}
#endif

		FHitResult NewHit;
		AddActorWorldOffset(GetWorld()->GetDeltaSeconds() * ProjOffset, true, &NewHit);
		if (NewHit.IsValidBlockingHit())
		{
			auto AwayDir = (Hit.ImpactNormal + NewHit.ImpactNormal).GetSafeNormal();
			auto Sideway = Up.Cross(AwayDir);
			auto SidewayOffset = WorldOffset.ProjectOnToNormal(Sideway);
			AddActorWorldOffset(GetWorld()->GetDeltaSeconds() * SidewayOffset, true, &NewHit);
		}
	}
}

void AQtaqCharacter::LookRotate(const FInputActionValue& Value)
{
	float Delta = GetWorld()->GetDeltaSeconds();
	auto V = Value.Get<FInputActionValue::Axis2D>();
	CameraArm->AddLocalRotation(Delta * VRotationSpeed * FRotator{V.Y, 0.0, 0.0});
	AddActorLocalRotation(Delta * HRotationSpeed * FRotator{0.0, V.X, 0.0});
}
