// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"

#include "QtaqCharacter.generated.h"

class UInputMappingContext;
struct FInputActionValue;
class UInputComponent;
class USkeletalMeshComponent;
class USpringArmComponent;
class UCameraComponent;
class UCapsuleComponent;
class UInputAction;

UCLASS()
class QTAQ_API AQtaqCharacter : public APawn
{
	GENERATED_BODY()

public:
	AQtaqCharacter();

	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

protected:
	virtual void BeginPlay() override;

private:
	void Move(const FInputActionValue& Value);
	void LookRotate(const FInputActionValue& Value);
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta=(AllowPrivateAccess="true"))
	TObjectPtr<USkeletalMeshComponent> SkeletalMesh;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta=(AllowPrivateAccess="true"))
	TObjectPtr<USpringArmComponent> CameraArm;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta=(AllowPrivateAccess="true"))
	TObjectPtr<UCameraComponent> Camera;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta=(AllowPrivateAccess="true"))
	TObjectPtr<UCapsuleComponent> Collision;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta=(AllowPrivateAccess="true"))
	TObjectPtr<UInputAction> MoveAction;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta=(AllowPrivateAccess="true"))
	TObjectPtr<UInputAction> LookRotateAction;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta=(AllowPrivateAccess="true"))
	TObjectPtr<UInputMappingContext> DefaultInputContext;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category=Movement, meta=(AllowPrivateAccess="true"))
	float Speed = 100.0;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category=Movement, meta=(AllowPrivateAccess="true"))
	float HRotationSpeed = 50.0;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category=Movement, meta=(AllowPrivateAccess="true"))
	float VRotationSpeed = 50.0;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category=Movement, AdvancedDisplay, meta=(AllowPrivateAccess="true"))
	bool ShouldDrawDebugVectorsOnCollision = false;

};
