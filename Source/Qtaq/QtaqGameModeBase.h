// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "QtaqGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class QTAQ_API AQtaqGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
